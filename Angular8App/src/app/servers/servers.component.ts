import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
allowNewServer: boolean = false;
serverName = 'TestServer';
userName = 'agadk';
  constructor() { 
  
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);

  }

  ngOnInit() {
  }
  
  onValueEnter(event: any){
    this.serverName = event.target.value;
  }

  setDisableButton(){
    return this.userName.length > 0;
  }

  clearFields(){
    this.userName = "";
  }
}
